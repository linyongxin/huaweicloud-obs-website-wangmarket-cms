# 项目简介

![image.png](https://images.gitee.com/uploads/images/2022/0530/084148_27b04528_429922.png)
本项目是OBS与网市场云建站CMS内容管理系统的深度整合，借助于华为云OBS对象存储提供静态网站托管功能，提供一个无技术门槛、可快速搭建100%自由度的网站。可做博客、文档、企业官网、电子简历…… 结合云计算的海量、安全、高可靠、低成本等优势，使企业都能享受云服务带来的技术便利。  
在做网站时，您无需任何学习，只需通过引导下一步、下一步、完成、即可完成整个操作。本工具自动帮您管理OBS桶。从创建桶、配置静态网站托管、首页、404页面、绑定域名……
 
# 系统优势
1. [**上云成本方面**，服务器没了~~一年也就少到可怜的几毛钱的存储及流量费。](https://e.gitee.com/leimingyun/doc/share/d88704307ba06edd/?sub_id=5624017)
1. **技术人员方面**，服务器运维及后端开发岗位（甚至前端工程师）也没了~~
1. **安全稳定方面**，数据持久性高达99.9999999999%、千万级并发、高可靠，彻底杜绝攻击、挂马等传统建站系统的安全隐患
1. **隐私担忧方面**，去中心化服务器，所有数据都在你自己当前的电脑上。在一些人眼里，自己手里的才是最安全的。我们就完全可以做到这样。只要你不想，谁也拿不到你当前网站的后台数据。
1. **上手使用方面**，按照内置的引导步骤，从安装、选模板、生成网站，遵循下一步，下一步，完成，网站就出来了。同时完善的扩展及开发文档，帮您真正用得起来，觉得好用。

# 功能简介

本系统提供CMS的一整套健全体系，以使小白用户无门槛使用。

* [模板体系](https://e.gitee.com/leimingyun/doc/share/d88704307ba06edd/?sub_id=5624625) ，所见皆可改
  * 全局变量、模板变量、模板页面、动态调用等，整套完善的模板体系。
  * 拿来即可直接导入使用的网站模板
  * 输入模型可自由定制各种类型的模板，不同栏目可自由定制不同的输入字段、每个输入项的备注说明等。
  * 完善的备份、还原、快速复制 等功能模块
  * 全部生成静态html页面，完美结合云存储的静态网站托管，将其优势发挥。
  * 极低门槛，详细的入门指引，懂一点基础的html(完全不需要懂后端开发、服务器知识)，你就能精通整个模板体系及精通整个系统的使用、服务客户。
* [栏目管理](https://e.gitee.com/leimingyun/doc/share/d88704307ba06edd/?sub_id=5625071)，可以自由管理网站中的栏目、属性、页面网址等
* [内容管理](https://e.gitee.com/leimingyun/doc/share/d88704307ba06edd/?sub_id=5628533)，文章内容添加、编辑、图片等文件上传操作。唯一的门槛是你要会打字。
* [域名绑定](https://e.gitee.com/leimingyun/doc/share/d88704307ba06edd/?sub_id=5624834)，可为自己做的网站，绑定上自己的域名。通过域名就能访问网站。另外你可以选择发布到香港可用区，无需备案就能用。

# 适用范围
当前为单机版本，仅适用于 Windows 64位操作系统，包含：  

* Windows 7
* Windows 8.1
* Windows 10
* Windows 11


另外还有开源的 **SAAS版本** : [https://gitee.com/mail_osc/wangmarket](https://gitee.com/mail_osc/wangmarket) 它可运行在1核1G的服务器上搭建上万个独立的网站！如果您想做多个、或您是建站公司想给客户做网站服务客户，您可选SAAS版本。    

# 服务场景

企业需要做一个自己的官网、个人做一个自己的简历/博客，不涉及到动态交互（如登录注册、下单购物等），只是单纯图文展示性质的网站，都可以使用本工具进行管理制作

# 使用步骤
打包好的应用软件下载地址：  
http://down.zvo.cn/wangmarket/wangmarket_serverless_windows_x64.zip  


### 1. 解压出来

如下图，注意，不要放到中文路径的文件夹中

![image.png](https://images.gitee.com/uploads/images/2022/0526/195959_b41ab3e5_429922.png)

### 2. 启动

双击 [ 启动.exe ]   ,进行启动 （有的会隐藏后缀，看不到 .exe ，那直接双击 [启动] 即可）

![image.png](https://images.gitee.com/uploads/images/2022/0526/200029_48dbe73b_429922.png)


### 3. 启动成功

看到如下图所示，即表示软件已成功启动。 启动成功后会自动打开下图中，右侧的浏览器，并自动进入安装界面，按提示安装即可

![image.png](https://images.gitee.com/uploads/images/2022/0526/200612_d5315eae_429922.png)


### 4. 更多使用

[![image.png](https://images.gitee.com/uploads/images/2022/0523/211503_b3ad8fa3_429922.png)更多帮助，请查阅 http://doc.serverless.wangmarket.zvo.cn](https://e.gitee.com/leimingyun/doc/share/d88704307ba06edd/?sub_id=5621467)


# 开源说明
本项目的核心 - CMS管理系统 的开源地址为 [https://gitee.com/mail_osc/wangmarket](https://gitee.com/mail_osc/wangmarket) 开源CMS建站中排名第二，拥有4k+ star关注。本项目是其在华为云OBS应用的衍生版本，将华为云的云存储-静态网站托管的优势最大化发挥出来，并能用于实际应用，让广大企业受益。  
禁止将本项目用于非法用途。 

# 价值体现
### 对要建站的个体
花十来分钟去熟悉它，充值5毛钱以应对存储及网站访问的流量消耗，这是付出的时间及费用成本。然后你就可以拥有一个自己的 企业官网/博客/个人简历/......

### 对建站公司
建站公司需要一个稳定、高效、安全的CMS系统来服务客户，同时又需要尽量付出最小的成本（上云及本身人员的成本），以将自身利润最大化。[传统建站与wangmarket CMS云建站的对比](https://e.gitee.com/leimingyun/doc/share/78815f06cd4554b7/?sub_id=7735555)

### 对华为云分销商
可通过本项目，进行扩展配置，将您的推广链接直接内置进去。当有用户按照步骤进行填写ak时，会自动通过您的推广链接进行注册华为云账户，关联上您。  
您在推广华为云时，如果要客户买服务器来上云，开通华为云账户，那受众群体可能非常窄。但如果您让客户知道可以5毛钱就能拥有自己的一个企业官网，无套路时，那可能就不一样了，哪个企业不需要官方网站呢？之前不需要，是因为成本高，不值得。那现在，以此方式，可能就比较愿意上云了。    
[本系统支持华为云经销商内置自己的推广邀请链接。内置您自己的推广邀请链接后将之重新打个压缩包，对外进行分发，当别的用户下载进行使用时，会按照步骤下一步下一步，开通华为云账户时，自然而然关联上您，成为您的下级。这对市场人员拓展新客户帮助非常大。如何操作可点击此处查看详细步骤](https://e.gitee.com/leimingyun/doc/share/d88704307ba06edd/?sub_id=5642145)


### 对云厂商
将云产品的优势充分发挥出来，降低使用门槛，对普通人来说不在是高高在上遥远不可触摸。  
同时其跟传统技术方案对比，碾压性的优势，极大降低用户上云门槛，给用户一个足够的，不好拒绝的上云的理由，更好的占领市场。

# 扩展开发
本项目为Java项目，使用JDK1.8，采用SpringBoot框架，拥有非常完善的面向于刚毕业同学级的入门文档，详细开发文档可查阅： [http://cms.zvo.cn](http://cms.zvo.cn)

# 版权说明
本项目采用Apache-2.0开源许可协议，您完全可以将它用于商业使用，而无需向我们支付任何费用。它的使命是帮更多的企业上云，借助云所带来的优势，普及信息化，起码，企业得有个网站吧...  
而用它来服务及造福更多企业，还需要您的支持。

# 问题反馈
[如果您在使用的过程中，发现不合理、非人类的操作、以及出现错误、发现bug，欢迎在此给我们提Issues，我们将第一时间回复并完善，好的产品离不开各位的反馈及支持，经过十余年的不断更新迭代，经过时间的考验，我们将努力做的更好。](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-obs-website-wangmarket-cms/issues/new)