<%@page import="com.xnx3.wangmarket.serverless.util.SystemPropertiesUtil"%>
<%@page import="com.xnx3.j2ee.util.SystemUtil"%>
<%@page import="com.xnx3.wangmarket.admin.G"%>
<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/wm/common/head.jsp">
	<jsp:param name="title" value="如何获取AK"/>
</jsp:include>


<style>
	
	.content{
		width: 600px;
		min-height:80%;
   margin: 0 auto;
   box-shadow: rgba(0, 0, 0, 0.06) 0px 1px 10px 2px;
   padding: 30px;
   margin-top: 50px;
	}
	.title{
		border-bottom: 1px solid #eee;
   padding-top: 20px;
   padding-left: 10px;
   padding-bottom: 20px;
   font-size: 28px;
   margin-bottom: 20px;
   text-align:center;
	}
	.content ul{
		padding-left: 20px;
	}
	.content ul li{
		list-style-type: decimal;
		padding-left:10px;
		padding-bottom:4px;
	}
	.content ul li img{
		max-width:250px;
		padding:4px;
		padding-left:40px;
	}
	.info{
		font-size:14px;
		line-height: 22px;
	}
	.info h2,h3,h4,h5{
	border-bottom: 1px solid #eee;
   padding-top: 23px;
   margin-bottom: 10px;
   padding-bottom: 5px;
	}
	
	@media only screen and (max-width: 700px) {
		.content{
			width:auto;
			margin-top: 0px;
			box-shadow: rgba(0, 0, 0, 0.06) 0px 0px 0px 0px;
		}
		
	}
	
	a{
		color: blue;
	}
	
	
	.markdown-body h4 {
  font-size: 1.25em;
}
img{
max-width:100%;
}
</style>

<div class="content">
	<div class="site-content markdown-body editormd-html-preview" id="content" style="">
    <h1 id="iw_title" style="">获取 Access Key Id、secret Access Key</h1>
    <div style="display:none;"><!-- 这里给爬虫抓的 --> #### 1. 创建一个账户 <a href="http://huawei.leimingyun.com" target="_blank">点击此处创建</a>。
![](//cdn.weiunity.com/site/1144/news/b771e670a39a4faf801f59a688f9007f.png)

#### 2. 进行实名认证 <a href="https://account.huaweicloud.com/usercenter/?region=af-south-1&amp;&amp;locale=zh-cn#/accountindex/realNameAuth" target="_blank">点击此处进行认证</a>。
企业认证、个人认证都行，必须认证完后才能使用。

#### 3. 点击我的凭证进入该模板
![](//cdn.weiunity.com/site/1144/news/b0ffa2105a9047f0acd8fa4e228434c2.png)
#### 4. 点击访问秘钥进入访问密码列表页面
![](//cdn.weiunity.com/site/1144/news/578d9ed24b224be09ce2ec55aedd5647.png)
#### 5. 如果没有访问秘钥的话点击新增获取一个新的访问秘钥
![](//cdn.weiunity.com/site/1144/news/0fbe72a72882471595fe7a098ffc4dca.png)
#### 6. 进行手机号身份验证
![](//cdn.weiunity.com/site/1144/news/ff47be0ec10043fca579d5bd58667ab1.png)
####完成手机号验证后，系统会自动下载一个文件，里面就是您刚刚获取的访问秘钥。将获得的秘钥信息通过插件进行填写。

#### 7. 最后，充值1毛钱
向账户中充值1毛钱，用于网站数据存储及访问的消耗。1毛钱，估计够你用几个月了  
一定留意华为云欠费的短信通知，如果欠费了网站将会无法访问，数据也会在一段时间内如果不充值会释放。</div>
    
<h4 id="h4-1-"><a name="1. 创建一个账户   点击此处创建 。" class="reference-link"></a><span class="header-link octicon octicon-link"></span>1. 创建一个账户 <a href="<%=SystemPropertiesUtil.getHuaweicloudRegUrl() %>" target="_blank">点击此处创建</a>。</h4><p><img src="//cdn.weiunity.com/site/1144/news/b771e670a39a4faf801f59a688f9007f.png" alt=""></p>
<h4 id="h4-2-"><a name="2. 进行实名认证   点击此处进行认证 。" class="reference-link"></a><span class="header-link octicon octicon-link"></span>2. 进行实名认证 <a href="https://account.huaweicloud.com/usercenter/?region=af-south-1&amp;&amp;locale=zh-cn#/accountindex/realNameAuth" target="_blank">点击此处进行认证</a>。</h4><p>企业认证、个人认证都行，必须认证完后才能使用。</p>
<h4 id="h4-3-"><a name="3. 点击我的凭证进入该模板" class="reference-link"></a><span class="header-link octicon octicon-link"></span>3. 点击我的凭证进入该模板</h4><p><img src="//cdn.weiunity.com/site/1144/news/b0ffa2105a9047f0acd8fa4e228434c2.png" alt=""></p>
<h4 id="h4-4-"><a name="4. 点击访问秘钥进入访问密码列表页面" class="reference-link"></a><span class="header-link octicon octicon-link"></span>4. 点击访问秘钥进入访问密码列表页面</h4><p><img src="//cdn.weiunity.com/site/1144/news/578d9ed24b224be09ce2ec55aedd5647.png" alt=""></p>
<h4 id="h4-5-"><a name="5. 如果没有访问秘钥的话点击新增获取一个新的访问秘钥" class="reference-link"></a><span class="header-link octicon octicon-link"></span>5. 如果没有访问秘钥的话点击新增获取一个新的访问秘钥</h4><p><img src="//cdn.weiunity.com/site/1144/news/0fbe72a72882471595fe7a098ffc4dca.png" alt=""></p>
<h4 id="h4-6-"><a name="6. 进行手机号身份验证" class="reference-link"></a><span class="header-link octicon octicon-link"></span>6. 进行手机号身份验证</h4><p><img src="//cdn.weiunity.com/site/1144/news/ff47be0ec10043fca579d5bd58667ab1.png" alt=""></p>
<h4 id="h4--"><a name="完成手机号验证后，系统会自动下载一个文件，里面就是您刚刚获取的访问秘钥。将获得的秘钥信息通过插件进行填写。" class="reference-link"></a><span class="header-link octicon octicon-link"></span>完成手机号验证后，系统会自动下载一个文件，里面就是您刚刚获取的访问秘钥。将获得的秘钥信息通过插件进行填写。</h4><h4 id="h4-7-1-"><a name="7. 最后，充值1毛钱" class="reference-link"></a><span class="header-link octicon octicon-link"></span>7. 最后，充值1毛钱</h4><p>向账户中充值1毛钱，用于网站数据存储及访问的消耗。1毛钱，估计够你用几个月了<br>一定留意华为云欠费的短信通知，如果欠费了网站将会无法访问，数据也会在一段时间内如果不充值会释放。</p>
</div>
	
	
	
</div>



<style>
.select{
	padding:20px;
}
.select>div{
	padding: 30px;
    border: 1px;
    border-style: solid;
    border-color: #d2d2d236;
}
.select div .title{
	font-size: 20px;
}
.select div .intro{
	font-size: 14px;
}


.lilist{
	padding-left: 40px;
}
.lilist>li{
	list-style: decimal;
}

.selectItem{
	cursor: pointer;
}
.selectItem:HOVER{
	border-color: #000000;
	background-color: #FAFAFA;
}
</style>

<script type="text/javascript">
/**
 * 设置区域。
 * param area 设置区域，传入如 cn-north-4
 */
function setArea(area){
	msg.loading("设置中...此过程耗时较长，请耐心等待");	//显示“更改中”的等待提示
	post("/plugin/serverless/create.json", { "area": area}, function(data){
		msg.close();	//关闭“更改中”的等待提示
		if(data.result != '1'){
			msg.failure(data.info);
		}else{
			wm.token.set(data.info);	//更新token
			msg.success('操作成功', function(){
				//标记安装成功
				window.location.href="/install/setLocalDomain.do";
			});
		}
	});
}
</script>

<jsp:include page="/wm/common/foot.jsp"></jsp:include> 