<%@page import="com.xnx3.wangmarket.admin.G"%>
<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/wm/common/head.jsp">
	<jsp:param name="title" value="欢迎使用网市场云建站系统"/>
</jsp:include>

  	<style>
html,body{
	height: 100%;
}


  		.content{
  			width: 600px;
  			min-height:80%;
		    margin: 0 auto;
		    box-shadow: rgba(0, 0, 0, 0.06) 0px 1px 10px 2px;
		    padding-left: 30px;
    padding-top: 50px;
    padding-right: 30px;
    height: 100%;
    box-sizing: border-box;
  		}
  		.title{
  			border-bottom: 1px solid #eee;
		    padding-top: 20px;
		    padding-left: 10px;
		    padding-bottom: 20px;
		    font-size: 28px;
		    margin-bottom: 20px;
  		}
  		.content ul{
  			padding-left: 20px;
  		}
  		.content ul li{
  			list-style-type: decimal;
  			padding-left:10px;
  			padding-bottom:4px;
  		}
  		.content ul li img{
  			max-width:250px;
  			padding:4px;
  			padding-left:40px;
  		}
		.info{
  			font-size:14px;
  			line-height: 22px;
  		}
  		.info h2,h3,h4,h5{
 			border-bottom: 1px solid #eee;
		    padding-top: 23px;
		    margin-bottom: 10px;
		    padding-bottom: 5px;
  		}
  		
  		@media only screen and (max-width: 700px) {
  			.content{
  				width:auto;
  				margin-top: 0px;
  				box-shadow: rgba(0, 0, 0, 0.06) 0px 0px 0px 0px;
  			}
  			
  		}
  		
  		a{
  			color: blue;
  		}
  	</style>
  	
    <div class="content">
    	<div class="title">
    		欢迎使用 网市场云建站系统 v<%=G.VERSION %> 
    		<span style="text-align:right;font-size: 14px;">(无服务器版本)</span>
    	</div>
    	
    	
    	<div style="padding-top:100px;; padding-bottom:110px; text-align:center;">
			<a href="/plugin/huaWeiYunServiceCreate/index.do" class="layui-btn layui-btn-primary" style="line-height: 0px;padding: 30px;font-size: 20px;">点击此处开始安装本系统</a>
			<br/>
			<div style="font-size:12px; padding-top:8px; ">
				<span style="color: gray;">交流QQ群22群：740332119</span> &nbsp;&nbsp; <span style="color: gray;">微信公众号：wangmarket</span>
				<!-- <a href="javascript:updateSystemConfig();" style="color: gray;">安装好后再次修改配置方式</a> -->
			</div>
			
		</div>
    	
    	<div class="info" style="bottom: 30px; width: 540px;color: gray;position: absolute;">
    		<h3>说明</h3>
    		本项目是OBS与网市场云建站CMS内容管理系统的深度整合，提供一个无技术门槛、可快速搭建100%自由度的网站。可做博客、文档、企业官网、电子简历…… 结合云计算的海量、安全、高可靠、低成本等优势，使企业都能享受云服务带来的技术便利。
    	</div>
    	
    <script>
    //修改系统相关配置，也就是安装的系统参数
    function updateSystemConfig(){
    	layer.open({
   		  title: '修改方式'
   		  ,content: '当您安装成功后，可使用账号 admin  密码 admin 进行登录，登录成功后，在 系统设置 - 系统变量 中，可进行修改相关配置参数。<br/>本次安装，便是针对系统变量的一些重要参数进行简单的引导设定。<br/>注意：看不懂的建议别随便改'
   		});
    }
    
    function huodong(){
    	msg.popups({
    		text:document.getElementById('huodong').innerHTML,
    		width:'50rem'
    	});
    }
    
    </script>
    
<jsp:include page="/wm/common/foot.jsp"></jsp:include> 
<style> /* 显示多语种切换 */ .translateSelectLanguage{ display:block; } </style>