package com.xnx3.wangmarket.serverless;

import org.springframework.stereotype.Component;

import com.xnx3.j2ee.pluginManage.PluginRegister;
import com.xnx3.j2ee.pluginManage.interfaces.DatabaseLoadFinishInterface;
import com.xnx3.j2ee.util.SpringUtil;
import com.xnx3.wangmarket.Authorization;
import com.xnx3.wangmarket.admin.G;

/**
 * 进行一些初始化
 * @author 管雷鸣
 *
 */
@PluginRegister(version="1.0", menuTitle = "")
public class Plugin implements DatabaseLoadFinishInterface{
	
	//进行一些初始化
	@Override
	public void databaseLoadFinish() {
		// TODO Auto-generated method stub
		System.out.println("init...");
		G.VERSION = "5.6.12";
		Authorization.setSoftType(3);
		Authorization.auth_id = "wangmarket_serverless_v5.6.12.20220727";
	}
	
}