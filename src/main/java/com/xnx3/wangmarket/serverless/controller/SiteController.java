package com.xnx3.wangmarket.serverless.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.xnx3.DateUtil;
import com.xnx3.exception.NotReturnValueException;
import com.xnx3.j2ee.Global;
import com.xnx3.j2ee.entity.User;
import com.xnx3.j2ee.service.SqlService;
import com.xnx3.j2ee.service.SystemService;
import com.xnx3.j2ee.service.UserService;
import com.xnx3.j2ee.util.ConsoleUtil;
import com.xnx3.j2ee.util.SpringUtil;
import com.xnx3.j2ee.util.SystemUtil;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.net.HttpResponse;
import com.xnx3.net.HttpUtil;
import com.xnx3.wangmarket.admin.entity.Site;
import com.xnx3.wangmarket.admin.entity.SiteUser;
import com.xnx3.wangmarket.admin.util.SessionUtil;
import com.xnx3.wangmarket.admin.util.TemplateAdminMenu.TemplateMenuEnum;
import com.xnx3.wangmarket.agencyadmin.entity.Agency;
import com.xnx3.wangmarket.plugin.base.controller.BasePluginController;

import cn.zvo.http.Http;
import cn.zvo.http.Response;

/**
 * 网站的一些相关
 * @author 管雷鸣
 */
@Controller(value="ServerLessSiteController")
@RequestMapping("/plugin/serverless/")
public class SiteController extends BasePluginController {
	@Resource
	private SqlService sqlService;
	@Resource
	private UserService userService;
	@Resource
	private SystemService systemService;
	
	
	/**
	 * 判断是否已生成整站
	 * @param 域名，传入如 www.guanleiming.com
	 * @return 是否已生成成功，已生成则是result=1
	 */
	@RequestMapping(value="isGenerateSite.json", method = RequestMethod.POST)
	@ResponseBody
	public BaseVO isGenerateSite(HttpServletRequest request, 
			@RequestParam(required = true , defaultValue="") String domain){
//		HttpUtil http = new HttpUtil();
//		HttpResponse hr = http.get("http://"+domain);
		Http http = new Http();
		Response res;
		try {
			res = http.get("http://"+domain);
		} catch (IOException e) {
			e.printStackTrace();
			return error("系统自检访问你网站域名时，未能成功访问。您可按照以下步骤进行排查：<br/><ul style=\"font-size:0.7rem; margin-top:0.5rem;\"><li>1. 检查您的域名是否做了CNAME解析</li><li>2. 您的域名CNAME解析有没有填写错误</li><li>3. 如果CNAME解析完全正常，可能是DNS缓存未更新，您只需请等待一会（比如十分钟）在进行尝试预览网站访问即可。</li></ul>");
		}
		if(res.getCode() == 0 && res.getContent() == null) {
			return error("系统自检访问你网站域名时，未能成功访问。您可按照以下步骤进行排查：<br/><ul style=\"font-size:0.7rem; margin-top:0.5rem;\"><li>1. 检查您的域名是否做了CNAME解析</li><li>2. 您的域名CNAME解析有没有填写错误</li><li>3. 如果CNAME解析完全正常，可能是DNS缓存未更新，您只需请等待一会（比如十分钟）在进行尝试预览网站访问即可。</li></ul>");
		}
		
//		if(hr.getCode() != 200) {
//			return error("系统自检访问你网站域名时，未能成功访问。响应码:"+hr.getCode());
//		}
		if(res.getCode() == 404) {
			return error("系统自检访问你网站域名时，发现您网站还没有首页。<br/><b>请点击右侧 [生成整站] 菜单，先来生成网站</b>");
		}
		
		return success("");
	}
}