package com.xnx3.j2ee.controller;

import java.awt.Font;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.xnx3.j2ee.util.ActionLogUtil;
import com.xnx3.j2ee.util.ConsoleUtil;
import com.xnx3.j2ee.util.SystemUtil;
import com.xnx3.DateUtil;
import com.xnx3.Lang;
import com.xnx3.exception.NotReturnValueException;
import com.xnx3.j2ee.Func;
import com.xnx3.j2ee.entity.User;
import com.xnx3.j2ee.service.SqlService;
import com.xnx3.j2ee.service.UserService;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.wangmarket.admin.entity.Site;
import com.xnx3.wangmarket.admin.entity.SiteUser;
import com.xnx3.wangmarket.admin.util.TemplateAdminMenu.TemplateMenuEnum;
import com.xnx3.wangmarket.agencyadmin.entity.Agency;
import com.xnx3.wangmarket.agencyadmin.entity.AgencyData;
import com.xnx3.wangmarket.agencyadmin.util.SessionUtil;

/**
 * 登录、注册 (wangmarket项目的改动)
 * @author 管雷鸣
 */
@Controller(value="WMLoginController")
@RequestMapping("/")
public class LoginController extends BaseController {
	@Resource
	private UserService userService;
	@Resource
	private SqlService sqlService;
	

	/**
	 * 验证码图片显示，直接访问此地址可查看图片
	 */
	@RequestMapping("/captcha${url.suffix}")
	public void captcha(HttpServletRequest request,HttpServletResponse response) throws IOException{
		ActionLogUtil.insert(request, "获取验证码显示");

		com.xnx3.media.CaptchaUtil captchaUtil = new com.xnx3.media.CaptchaUtil();
	    captchaUtil.setCodeCount(5);                   //验证码的数量，若不增加图宽度的话，只能是1～5个之间
	    captchaUtil.setFont(new Font("Fixedsys", Font.BOLD, 21));    //验证码字符串的字体
	    captchaUtil.setHeight(18);  //验证码图片的高度
	    captchaUtil.setWidth(110);      //验证码图片的宽度
//	    captchaUtil.setCode(new String[]{"我","是","验","证","码"});   //如果对于数字＋英文不满意，可以自定义验证码的文字！
	    com.xnx3.j2ee.util.CaptchaUtil.showImage(captchaUtil, request, response);
	}
	

	/**
	 * 登录
	 */
	@RequestMapping("login${url.suffix}")
	public String login(HttpServletRequest request, Model model){
		if(getUser() != null){
			ActionLogUtil.insert(request, "进入登录页面", "已经登录成功，无需再登录，进行跳转");
			return redirect(com.xnx3.wangmarket.admin.Func.getConsoleRedirectUrl());
		}
		if(SystemUtil.get("IW_AUTO_INSTALL_USE").equals("true")){
			//尚未安装，进入安装界面
			return redirect("install/index.do");
		}
		ActionLogUtil.insert(request, "进入登录页面");
		

		//验证码校验通过
		BaseVO baseVO = userService.loginForUserId(request, 243);
		if(baseVO.getResult() - BaseVO.SUCCESS == 0){
			//得到当前登录的用户的信息
			User user = sqlService.findById(User.class, 243);
			//得到当前用户，在网市场中，user的扩展表 site_user 的信息
			SiteUser siteUser = sqlService.findById(SiteUser.class, user.getId());
			//缓存进session
			com.xnx3.wangmarket.admin.util.SessionUtil.setSiteUser(siteUser);
			//得到上级的代理信息
			Agency parentAgency = sqlService.findAloneBySqlQuery("SELECT * FROM agency WHERE userid = 392", Agency.class);
			SessionUtil.setParentAgency(parentAgency);
			
			Site site = sqlService.findAloneBySqlQuery("SELECT * FROM site WHERE userid = "+getUserId()+" ORDER BY id DESC", Site.class);
			//将拥有所有功能的管理权限，将功能菜单全部遍历出来，赋予这个用户
			Map<String, String> menuMap = new HashMap<String, String>();
			for (TemplateMenuEnum e : TemplateMenuEnum.values()) {
				menuMap.put(e.id, "1");
			}
			SessionUtil.setSiteMenuRole(menuMap);
			//将所管理的网站加入session缓存
			SessionUtil.setSite(site);
			
			return redirect("template/index.do");
		}else {
			return error(model, baseVO.getInfo());
		}
	}
	

	
	/**
	 * 退出登录状态
	 */
	@RequestMapping(value="logout.json", method = RequestMethod.POST)
	@ResponseBody
	public BaseVO logout(HttpServletRequest request,Model model){
		SessionUtil.logout();
		return success();
	}
	
	/**
	 * 退出登录状态，兼容旧版本
	 */
	@RequestMapping(value="logout.do", method = RequestMethod.POST)
	@ResponseBody
	public BaseVO logout_old(HttpServletRequest request,Model model){
		SessionUtil.logout();
		return success();
	}
}
